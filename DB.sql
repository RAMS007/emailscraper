-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.50-log - MySQL Community Server (GPL) by Remi
-- Операционная система:         Linux
-- HeidiSQL Версия:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица emailscraper.ItemsTable
CREATE TABLE IF NOT EXISTS `ItemsTable` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` bigint(20) unsigned NOT NULL,
  `number` bigint(20) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` bigint(20) unsigned NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `TotalPrice` decimal(10,2) NOT NULL,
  `PriceWithTaxes` decimal(10,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `itemstable_orderid_index` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица emailscraper.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица emailscraper.OrdersTable
CREATE TABLE IF NOT EXISTS `OrdersTable` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `refNumber` bigint(20) unsigned NOT NULL,
  `userEmail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `emailId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hashedEmailId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('created','paid','canceled','shipped') COLLATE utf8mb4_unicode_ci NOT NULL,
  `OrderTotal` decimal(10,2) NOT NULL,
  `OrderTotalWithTaxes` decimal(10,2) NOT NULL DEFAULT '0.00',
  `paymentHash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hashValue` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица emailscraper.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица emailscraper.PaymentResponse
CREATE TABLE IF NOT EXISTS `PaymentResponse` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trnApproved` tinyint(1) DEFAULT NULL,
  `trnId` bigint(20) unsigned DEFAULT '0',
  `messageId` int(10) unsigned DEFAULT '0',
  `messageText` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `authCode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `responseType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `trnAmount` decimal(10,2) DEFAULT '0.00',
  `trnDate` datetime DEFAULT NULL,
  `trnOrderNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `trnLanguage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `trnCustomerName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `trnEmailAddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `trnPhoneNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avsProcessed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `avsId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `avsResult` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `avsAddrMatch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `avsPostalMatch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `avsMessage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `cvdId` int(10) unsigned DEFAULT '0',
  `cardType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `trnType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `paymentMethod` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `ref1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hashValue` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица emailscraper.ProvincesTable
CREATE TABLE IF NOT EXISTS `ProvincesTable` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица emailscraper.ShippingBillingInformation
CREATE TABLE IF NOT EXISTS `ShippingBillingInformation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` bigint(20) unsigned NOT NULL,
  `FirstNameB` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LastNameB` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ShippingAdressB` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CountryB` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ProvinceB` int(10) unsigned NOT NULL,
  `PostCodeB` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FirstNameS` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LastNameS` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ShippingAdressS` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CountryS` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ProvinceS` int(10) unsigned NOT NULL,
  `PostCodeS` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица emailscraper.ShippingRate
CREATE TABLE IF NOT EXISTS `ShippingRate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lowPrice` decimal(14,4) NOT NULL,
  `highPrice` decimal(14,4) NOT NULL,
  `shippingPrice` decimal(14,4) NOT NULL,
  `provinceId` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shippingrate_provinceid_foreign` (`provinceId`),
  CONSTRAINT `shippingrate_provinceid_foreign` FOREIGN KEY (`provinceId`) REFERENCES `ProvincesTable` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица emailscraper.Taxes
CREATE TABLE IF NOT EXISTS `Taxes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `canadianTax` decimal(14,4) NOT NULL,
  `provincialTax` decimal(14,4) NOT NULL,
  `provinceId` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `taxes_provinceid_foreign` (`provinceId`),
  CONSTRAINT `taxes_provinceid_foreign` FOREIGN KEY (`provinceId`) REFERENCES `ProvincesTable` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица emailscraper.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`(50))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Экспортируемые данные не выделены.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
