<?php

namespace App\Http\Controllers;

use App\OrdersTable;
use App\ProvincesTable;
use App\Shipping;
use App\Taxes;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard()
    {
        $domain = env('APP_URL');
        $orders = OrdersTable::all(['id', 'refNumber', 'userEmail', 'status', 'hashedEmailId']);
        return view('admin.dashboard', ['Orders' => $orders, 'domain' => $domain, 'page' => 'Dashboard']);
    }

    public function getOrderDetails($orderId)
    {
        $order = OrdersTable::find($orderId);
        if (!empty($order)) {
            return response()->json(['error' => false, 'order' => $order]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Order not found']);
        }
    }

    public function getAllOrderDetails($orderId)
    {
        $order = OrdersTable::find($orderId);
        if (!empty($order)) {
            return view('admin.orderDetail', ['order' => $order, 'page' => 'Dashboard']);
        } else {
            return redirect('/admin');
        }
    }


    public function updateOrderDetails($orderId, Request $request)
    {
        try {
            $msg = '';
            if (empty($request->refNumber)) {
                $msg = 'Please enter refNumber';
            }
            if (empty($request->userEmail)) {
                $msg = 'Please enter userEmail';
            }
            if (empty($request->note)) {
                $msg = 'Please enter note';
            }

            if (empty($request->status)) {
                $msg = 'Please enter status';
            }
            if (empty($request->OrderTotal)) {
                $msg = 'Please enter OrderTotal';
            }

            if (!empty($msg)) {

                return response()->json(['error' => true, 'msg' => $msg]);
            }


            $order = OrdersTable::find($orderId);
            if (!empty($order)) {
                $order->refNumber = $request->refNumber;
                $order->userEmail = $request->userEmail;
                $order->note = $request->note;
                $order->status = $request->status;
                $order->OrderTotal = $request->OrderTotal;
                $order->save();
                return response()->json(['error' => false, 'msg' => 'Updated']);
            } else {
                return response()->json(['error' => true, 'msg' => 'Order not found']);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => 'Not updated']);
        }

    }

    public function deleteOrder($orderId)
    {
        $order = OrdersTable::find($orderId);
        if (!empty($order)) {
            $order->delete();
            return response()->json(['error' => false, 'msg' => 'Deleted']);
        } else {
            return response()->json(['error' => true, 'msg' => 'Order not found']);
        }
    }


    public function getProvinces()
    {
        $provinces = ProvincesTable::all();
        if (!empty($provinces)) {
            //     $items = $order->items ;
            //     $items = $order->items()->get(); ;
            return view('admin.provinces', ['provinces' => $provinces, 'page' => 'Provinces']);
        } else {
            return redirect('/admin');
        }
    }


    public function getAllProvinceDetails($provinceId)
    {
        $province = ProvincesTable::find($provinceId);
        if (!empty($province)) {
            return response()->json(['error' => false, 'province' => $province]);
        } else {
            return redirect('/admin');
        }
    }


    public function updateProvinceDetails($provinceId, Request $request)
    {
        $province = ProvincesTable::find($provinceId);
        if (!empty($province)) {
            $province->Country = $request->Country;
            $province->Province = $request->Province;
            $province->save();
            return response()->json(['error' => false, 'msg' => 'Updated']);
        } else {
            return response()->json(['error' => true, 'msg' => 'Province not found']);
        }
    }

    public function deleteProvince($provinceId)
    {
        $province = ProvincesTable::find($provinceId);
        if (!empty($province)) {
            $province->delete();
            return response()->json(['error' => false, 'msg' => 'Deleted']);
        } else {
            return response()->json(['error' => true, 'msg' => 'Province not found']);
        }
    }

    public function createProvince(Request $request)
    {
        $province = ProvincesTable::create(['Country' => $request->Country, 'Province' => $request->Province]);
        if (!empty($province)) {

            return response()->json(['error' => false, 'msg' => 'Created']);
        } else {
            return response()->json(['error' => true, 'msg' => 'Province not found']);
        }
    }


    public function getTaxes()
    {
        $taxes = Taxes::all();
        if (!empty($taxes)) {
            return view('admin.taxes', ['taxes' => $taxes, 'page' => 'Taxes']);
        } else {
            return redirect('/admin');
        }
    }

    public function getAllTaxDetails($taxId)
    {
        $tax = Taxes::find($taxId);
        if (!empty($tax)) {
            return response()->json(['error' => false, 'tax' => $tax]);
        } else {
            return redirect('/admin');
        }
    }

    public function updateTaxDetails($taxId, Request $request)
    {
        $tax = Taxes::find($taxId);
        if (!empty($tax)) {
            $tax->canadianTax = $request->canadianTax;
            $tax->provincialTax = $request->provincialTax;

            $tax->provinceId = $request->Province;
            $tax->save();
            return response()->json(['error' => false, 'msg' => 'Updated']);
        } else {
            return response()->json(['error' => true, 'msg' => 'Tax not found']);
        }
    }

    public function deleteTax($taxId)
    {
        $tax = Taxes::find($taxId);
        if (!empty($tax)) {
            $tax->delete();
            return response()->json(['error' => false, 'msg' => 'Deleted']);
        } else {
            return response()->json(['error' => true, 'msg' => 'Tax not found']);
        }
    }


    public function createTax(Request $request)
    {
        $tax = Taxes::create(['canadianTax' => $request->canadianTax, 'provincialTax' => $request->provincialTax, 'provinceId' => $request->Province]);
        if (!empty($tax)) {

            return response()->json(['error' => false, 'msg' => 'Created']);
        } else {
            return response()->json(['error' => true, 'msg' => 'Tax not created']);
        }
    }

    public function getShipping()
    {

        $shipping = Shipping::all();
        if (!empty($shipping)) {
            return view('admin.shipping', ['shipping' => $shipping, 'page' => 'Shipping']);
        } else {
            return redirect('/admin');
        }

    }


    public function createShiipping(Request $request)
    {
        $shipping = Shipping::create(['highPrice' => $request->highPrice, 'lowPrice' => $request->lowPrice, 'shippingPrice' => $request->shippingPrice, 'provinceId' => $request->Province]);
        if (!empty($shipping)) {
            return response()->json(['error' => false, 'msg' => 'Created']);
        } else {
            return response()->json(['error' => true, 'msg' => 'Tax not created']);
        }
    }

    public function getAllshippingDetails($shippingId)
    {
        $shipping = Shipping::find($shippingId);
        if (!empty($shipping)) {
            return response()->json(['error' => false, 'shipping' => $shipping]);
        } else {
            return redirect('/admin');
        }
    }

    public function updateshippingDetails($shippingId, Request $request)
    {
        $shipping = Shipping::find($shippingId);
        if (!empty($shipping)) {
            $shipping->lowPrice = $request->lowPrice;
            $shipping->highPrice = $request->highPrice;
            $shipping->shippingPrice = $request->shippingPrice;
            $shipping->provinceId = $request->Province;
            $shipping->save();
            return response()->json(['error' => false, 'msg' => 'Updated']);
        } else {
            return response()->json(['error' => true, 'msg' => 'Tax not found']);
        }
    }

    public function deleteshipping($shippingId)
    {
        $shipping = Shipping::find($shippingId);
        if (!empty($shipping)) {
            $shipping->delete();
            return response()->json(['error' => false, 'msg' => 'Deleted']);
        } else {
            return response()->json(['error' => true, 'msg' => 'Tax not found']);
        }
    }
}
