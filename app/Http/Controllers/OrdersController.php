<?php

namespace App\Http\Controllers;

use App\ItemsTable;
use App\OrdersTable;
use App\ProvincesTable;
use App\Shipping;
use App\ShippingBillingInformation;
use App\Taxes;
use Illuminate\Http\Request;
use PhpImap\Mailbox as ImapMailbox;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;
use PhpImap\Mailbox;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrdersController extends Controller
{

    public function ProcessEmails()
    {

// 4. argument is the directory into which attachments are to be saved:
        $mailbox = new Mailbox('{plesk1.b367.ca:993/imap/ssl/novalidate-cert}INBOX', 'pae@bolle-agence.ca', 'uJr72&4m', __DIR__);
        $mailsIds = $mailbox->searchMailbox('ALL');
        if (!$mailsIds) {
            die('Mailbox is empty');
        }

        Log::info('count mails' . count($mailsIds));

        foreach ($mailsIds as $mailId) {
// Get the first message and save its attachment(s) to disk:
            $mail = $mailbox->getMail($mailId);
            $mailText = strip_tags($mail->textHtml);
            $mailText = str_replace('&nbsp;', '', $mailText);
            $mailText = str_replace("\r\n\r\n", "\r\n", $mailText);
            $mailText = str_replace("\r\n\r\n", "\r\n", $mailText);
            $mailText = str_replace("\r\n\r\n", "\r\n", $mailText);
            $mailText = str_replace("\r\n\r\n", "\r\n", $mailText);
            $mailText = str_replace("\r\n\r\n", "\r\n", $mailText);
            $mailText = str_replace("\r\n\r\n", "\r\n", $mailText);
            $mailText = str_replace("\r\n\r\n", "\r\n", $mailText);
            $mailText = str_replace("\r\n\r\n", "\r\n", $mailText);
            $mailText = str_replace("\r\n\r\n", "\r\n", $mailText);
            $mailText = trim($mailText);


            preg_match('/^(.*?)[\r\n]+/is', $mailText, $matches);
            if (!isset($matches[1])) {
                $r = $mailbox->moveMail($mailId, 'INBOX.BAD');
                continue;
            }
            $note = $matches[1];

            preg_match('/MRP Id #([0-9]+)/is', $mailText, $matches);
            $refNumber = $matches[1];

            $emailId = $mail->messageId;
            $hashedemailId = md5($emailId);

            preg_match('/Email:(.*?)\n/is', $mailText, $matches);
            $customerEmail = trim($matches[1]);

            preg_match('/Total:.*?\$([0-9\.]+) \(does not include Sales tax or applicable environmental\/disposal fees\)/is', $mailText, $matches);
            $OrderTotal = trim($matches[1]);

            preg_match_all('/(Item #.*?Item Total: \$[0-9. ]+)/is', $mailText, $matches);

            $newOrder = OrdersTable::create([
                'refNumber' => trim($refNumber),
                'emailId' => trim($emailId),
                'hashedEmailId' => trim($hashedemailId),
                'note' => trim($note),
                'userEmail' => $customerEmail,
                'OrderTotal' => $OrderTotal
            ]);

            for ($i = 0; $i < count($matches[1]); $i++) {

                preg_match('/Item #([0-9 ]+)[\r\n]+(.*?)[\r\n]+Description:(.*?)[\r\n]+Qty:([0-9 ]+)Location:(.*?)[\r\n]+Price: \$([0-9\. ]+)[\r\n]+Item total: \$([0-9\. ]+)/i', $matches[1][$i], $itemMatch);

                $item = ItemsTable::create([
                    'number' => trim($itemMatch[1]),
                    'title' => trim($itemMatch[2]),
                    'description' => trim($itemMatch[3]),
                    'quantity' => trim($itemMatch[4]),
                    'price' => trim($itemMatch[6]),
                    'TotalPrice' => trim($itemMatch[7]),
                    'orderId' => $newOrder->id,
                ]);

            }
            $r = $mailbox->moveMail($mailId, 'INBOX.PROCESSED');
        }
    }


    public function showBilling(Request $request)
    {

        $Order = OrdersTable::where('hashedEmailId', $request->token)->first();
        if (empty($Order)) {
            abort(404);
        }
        return view('billing', ['Order' => $Order]);
    }


    public function getProvinces(Request $request)
    {
        $allProvinces = ProvincesTable::where('Country', $request->country)->get();
        return response()->json($allProvinces);
    }



    ///https://dev.na.bambora.com/docs/forms/link_builder/#link
    //https://web.na.bambora.com/admin/sDefault.asp  config->payment form


    public function makePayment(Request $request)
    {
        $basePaymentUrl = 'https://web.na.bambora.com/scripts/payment/payment.asp';
        $merchantId = env('BEANSTREAM_MERCHANT_ID');
        $hashKey = env('BEANSTREAM_HASH');
        $order = OrdersTable::where('hashedEmailId', $request->token)->first();
        $HashForFindingOrder = md5($order->userEmail . $order->id); // use for finding our order

        $approvedLink = env('APP_URL') . 'payments/result/' . $HashForFindingOrder;
        $declineLink = env('APP_URL') . 'payments/result/' . $HashForFindingOrder;


        $stringToHash = 'merchant_id=' . $merchantId;
        $stringToHash .= '&trnAmount=' . $request->amount;

        $ShipProvince = ProvincesTable::find($request->shippingstate);
        $BillingProvince = isset($request->shippingstateB) ? ProvincesTable::find($request->shippingstateB) : 0;
        $BillingName = urlencode($request->firstnameB . ' ' . $request->lastnameB);
        $ShipName = urlencode($request->firstname . ' ' . $request->lastname);
        if (isset($order->userEmail) AND !empty($order->userEmail)) {
            $email = $order->userEmail;
        }
        $ShipAdress = urlencode($request->shippingaddress);
        $BillingAdress = urlencode($request->shippingaddressB);
        $ShipPostCode = urlencode($request->postcode);
        $BillingPostCode = urlencode($request->postcodeB);

        $ShipCountry = urlencode($request->country);
        $BillingCountry = urlencode($request->countryB);

        if (isset($request->billinginformation) and ($request->billinginformation == true)) {
            $BillingProvince = $ShipProvince;
            $BillingName = $ShipName;
            $BillingAdress = $ShipAdress;
            $BillingPostCode = $ShipPostCode;
            $BillingCountry = $ShipCountry;
            //(['firstnameB'=> $request->firstname, 'lastnameB'=> $request->lastname]);
        }


        $stringToHash .= '&ordName=' . $BillingName;
        $stringToHash .= '&ordEmailAddress=' . $email;
        $stringToHash .= '&ordAddress1=' . $BillingAdress;
//          $stringToHash.=   '&ordCity=bcity'
        //       $stringToHash .= '&ordProvince=' . $BillingProvince->Province;
//        $stringToHash .= '&ordPostalCode=' . $BillingPostCode;
//        $stringToHash .= '&ordCountry=' . $BillingCountry;
        $stringToHash .= '&shipName=' . $ShipName;
        $stringToHash .= '&shipEmailAddress=' . $email;
//          $stringToHash.=   '&shipPhoneNumber=sphone'
        $stringToHash .= '&shipAddress1=' . $ShipAdress;
//          $stringToHash.=   '&shipCity=scity'
        //       $stringToHash .= '&shipProvince=' . $ShipProvince->Province;
//        $stringToHash .= '&shipPostalCode=' . $ShipPostCode;
//        $stringToHash .= '&shipCountry=' . $ShipCountry;
        $stringToHash .= '&approvedPage=' . $approvedLink;
        $stringToHash .= '&declinedPage=' . $declineLink;


        $hash = md5($stringToHash . $hashKey);
        $formUrl = $basePaymentUrl . '?' . $stringToHash . '&hashValue=' . $hash;
        $order->paymentHash = $hash;
        $order->OrderTotalWithTaxes = $request->amount;
        $order->hashValue = $HashForFindingOrder;
        $order->save();

        ShippingBillingInformation::create([

            'orderId' => $order->id,
            'FirstNameB' => isset($request->firstnameB) ? $request->firstnameB : $request->firstname,
            'LastNameB' => isset($request->lastnameB) ? $request->lastnameB : $request->lastname,
            'ShippingAdressB' => $BillingAdress,
            'CountryB' => $BillingCountry,
            'ProvinceB' => $BillingProvince->id,
            'PostCodeB' => $BillingPostCode,

            'FirstNameS' => $request->firstname,
            'LastNameS' => $request->lastname,
            'ShippingAdressS' => $ShipAdress,
            'CountryS' => $ShipCountry,
            'ProvinceS' => $ShipProvince->id,
            'PostCodeS' => $ShipPostCode,
            'hash' => $hash
        ]);

        return redirect($formUrl);
    }


    public function getTaxes()
    {

        $taxes = Taxes::all();
        $resultTax = [];
        foreach ($taxes as $tax) {
            $resultTax[$tax->provinceId] = ['canadianTax' => $tax->canadianTax, 'provincialTax' => $tax->provincialTax];
        }
        return response()->json(['error' => false, 'taxes' => $resultTax]);
    }


    public function getShipping()
    {
        $shipping = Shipping::all();
        return response()->json(['error' => false, 'shipping' => $shipping]);
    }

}
