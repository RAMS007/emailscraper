<?php

namespace App\Http\Controllers;

use App\OrdersTable;
use App\PaymentResponse;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{

    public function processResponse(Request $request, $ourHash)
    {
        $result = $request->all();
        $date = strtotime($result['trnDate']);
        if (empty($date)) {
            $date = time();
        }
        $result['trnDate'] = date('Y-m-d H:i:s', $date);
        $record = PaymentResponse::create($result);
        $order = OrdersTable::where('hashValue', $ourHash)->first();
        if (empty($order)) {
            abort(403, 'Order not found');
        }


        if ($result['trnApproved'] == 1) {
            $order->status = 'paid';
            $order->save();
            //@todo mail customer


            return redirect('/payments/success');
        } else {
            return redirect('/payments/fail');
        }
    }


}
