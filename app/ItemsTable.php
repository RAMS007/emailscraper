<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsTable extends Model
{
    protected $table='ItemsTable';
    protected $guarded=['id'];
}
