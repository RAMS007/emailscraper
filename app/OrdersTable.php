<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersTable extends Model
{
  protected $table='OrdersTable';
    protected $guarded=['id'];


    public function Items (){

        return $this->hasMany('App\ItemsTable','orderId','id');

    }
}
