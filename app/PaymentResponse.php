<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentResponse extends Model
{
    protected $table='PaymentResponse';
    protected $guarded=['id'];
}
