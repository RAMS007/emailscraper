<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvincesTable extends Model
{
    protected $table='ProvincesTable';
    protected $guarded=['id'];
}
