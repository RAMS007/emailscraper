<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $table='ShippingRate';
    protected $guarded=['id'];

    public function Province (){

        return $this->hasOne('App\ProvincesTable','id','provinceId');

    }

}
