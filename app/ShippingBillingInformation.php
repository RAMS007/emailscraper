<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingBillingInformation extends Model
{
    protected $table='ShippingBillingInformation';
    protected $guarded=['id'];
}
