<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxes extends Model
{
    protected $table='Taxes';
    protected $guarded=['id'];

    public function Province (){

        return $this->hasOne('App\ProvincesTable','id','provinceId');

    }
}
