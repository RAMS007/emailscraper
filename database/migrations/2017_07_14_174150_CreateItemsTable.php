<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ItemsTable', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('orderId');
            $table->unsignedBigInteger('number');
            $table->string('title');
            $table->string('description');
            $table->unsignedBigInteger('quantity');
            $table->decimal('price',10,2);
            $table->decimal('TotalPrice',10,2);
            $table->decimal('PriceWithTaxes',10,2)->default(0);
            $table->timestamps();
            $table->index('orderId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ItemsTable');
    }
}
