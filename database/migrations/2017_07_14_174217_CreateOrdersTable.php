<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('OrdersTable', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('refNumber');
            $table->string('userEmail')->default('');
            $table->string('emailId');
            $table->string('hashedEmailId');
            $table->text('note');
            $table->enum('status',['created', 'paid', 'canceled', 'shipped']);
            $table->decimal('OrderTotal',10,2);
            $table->decimal('OrderTotalWithTaxes',10,2)->default(0);
            $table->string('paymentHash')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('OrdersTable');
    }
}
