<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Taxes', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('canadianTax',5,2);
            $table->decimal('provincialTax',5,2);
            $table->unsignedInteger('provinceId');
            $table->timestamps();
            $table->foreign('provinceId')->references('id')->on('ProvincesTable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Taxes');
    }
}
