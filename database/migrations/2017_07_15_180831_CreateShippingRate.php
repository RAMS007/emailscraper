<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingRate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ShippingRate', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('lowPrice',10,2);
            $table->decimal('highPrice',10,2);
            $table->decimal('shippingPrice',10,2);
            $table->unsignedInteger('provinceId');
            $table->timestamps();
            $table->foreign('provinceId')->references('id')->on('ProvincesTable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ShippingRate');
    }
}
