<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PaymentResponse', function (Blueprint $table) {
            $table->increments('id');
$table->boolean('trnApproved');
        $table->unsignedBigInteger('trnId')->default(0);
            $table->unsignedInteger('messageId')->default(0);
            $table->string('messageText')->default('');
            $table->string('authCode')->default('');
            $table->string('responseType')->default('');
  $table->decimal('trnAmount',10,2)->default(0);
    $table->dateTime('trnDate');
            $table->unsignedBigInteger('trnOrderNumber')->default(0);
            $table->string('trnLanguage')->default('');
            $table->string('trnCustomerName')->default('');
            $table->string('trnEmailAddress')->default('');
            $table->string('trnPhoneNumber')->nullable();


            $table->string('avsProcessed')->default('');
            $table->string('avsId')->default('');
            $table->string('avsResult')->default('');
            $table->string('avsAddrMatch')->default('');
            $table->string('avsPostalMatch')->default('');
            $table->string('avsMessage')->default('');
            $table->unsignedInteger('cvdId')->default(0);
            $table->string('cardType')->default('');
            $table->string('trnType')->default('');
            $table->string('paymentMethod')->default('');
            $table->string('ref1')->nullable();
            $table->string('ref2')->nullable();
            $table->string('ref3')->nullable();
            $table->string('ref4')->nullable();
            $table->string('ref5')->nullable();
            $table->string('hashValue')->default('');







            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PaymentResponse');
    }
}
