<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingBillingInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ShippingBillingInformation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('orderId');
            $table->string('FirstNameB');
            $table->string('LastNameB');
            $table->string('ShippingAdressB');
            $table->string('CountryB');
            $table->unsignedInteger('ProvinceB');
            $table->string('PostCodeB');

            $table->string('FirstNameS');
            $table->string('LastNameS');
            $table->string('ShippingAdressS');
            $table->string('CountryS');
            $table->unsignedInteger('ProvinceS');
            $table->string('PostCodeS');



            $table->string('hash');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ShippingBillingInformayion');
    }
}
