<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        h2.bg-success {
            padding: 15px;
        }

        .required-lbl {
            color: red;
        }

        label[for="billinginformation"] {
            display: inline;
            float: left;
            margin: 0px 45px 0px 0px;
        }

        .card-expiry {
            padding-left: 0px;
        }

        .confirm-btn {
            float: right;
        }

        .bg-primary {
            padding: 10px;
        }

        label {
            margin-bottom: 0px;
        }
    </style>
</head>
<body>
<!-- Fixed navbar -->
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                @if($page=='Dashboard')
                    <li class="active"><a href="/admin">Dashboard</a></li>
                @else
                    <li><a href="/admin">Dashboard</a></li>
                @endif

                @if($page=='Provinces')
                    <li class="active"><a href="/admin/provinces">Provinces</a></li>
                @else
                    <li><a href="/admin/provinces">Provinces</a></li>
                @endif


                @if($page=='Taxes')
                    <li class="active"><a href="/admin/taxes">Taxes</a></li>
                @else
                    <li><a href="/admin/taxes">Taxes</a></li>
                @endif


                @if($page=='Shipping')
                    <li class="active"><a href="/admin/shipping">Shipping</a></li>
                @else
                    <li><a href="/admin/shipping">Shipping</a></li>
                @endif

                <li><a href="/ProcessEmails">RUN cronjob</a></li>
            </ul>

        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="container">

    @yield('container')


</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/jquery-3.2.1.min.js"><\/script>')</script>
<script src="/js/bootstrap.min.js"></script>
<link href="/css/toastr.min.css" rel="stylesheet"/>
<link href="/css/jquery.dataTables.min.css" rel="stylesheet"/>

<script src="/js/toastr.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>


@yield('script')

</body>
</html>
