@extends('admin.base')

@section('container')


    <h1>Order list</h1>

    <table class="table table-hover datatable">
        <thead>
        <tr>
            <th>id</th>
            <th>refNumber</th>
            <th>userEmail</th>
            <th>status</th>
            <th>userLink</th>
            <th>action</th>

        </tr>
        </thead>
        <tbody>

        @foreach ($Orders as $order)
            <tr>
                <td>
                    {{$order->id}}
                </td>
                <td>
                    {{$order->refNumber}}
                </td>
                <td>
                    {{$order->userEmail}}
                </td>
                <td>
                    {{$order->status}}
                </td>
                <td>
                    <a href="{{$domain}}/billing?token={{$order->hashedEmailId}}" target="_blank">Link </a>
                </td>

                <td data-id="{{$order->id}}">
                    <a href="admin/order/details/{{$order->id}}" class="btn btn-info edit-item">Details</a>
                    <button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button>
                    <button class="btn btn-danger remove-item">Delete</button>
                </td>

            </tr>


        @endforeach


        </tbody>
    </table>





    <!-- Edit Item Modal -->
    <div class="modal fade" id="edit-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit order</h4>
                </div>

                <div class="modal-body">
                    <form data-toggle="validator" action="admin/order" method="put" id="editForm">
                        <input type="hidden" name="id" class="edit-id">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label" for="refNumber">refNumber:</label>
                            <input type="text" name="refNumber" class="form-control"
                                   data-error="Please enter refNumber." required/>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="userEmail">userEmail:</label>
                            <input type="text" name="userEmail" class="form-control"
                                   data-error="Please enter userEmail." required/>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="note">note:</label>
                            <textarea name="note" class="form-control" data-error="Please enter note."
                                      required> </textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="status">status:</label>
                            <select name="status" class="form-control">
                                <option value="created">created</option>
                                <option value="paid">paid</option>
                                <option value="canceled">canceled</option>
                                <option value="shipped">shipped</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="OrderTotal">OrderTotal:</label>
                            <input type="text" name="OrderTotal" class="form-control"
                                   data-error="Please enter OrderTotal." required/>
                            <div class="help-block with-errors"></div>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-success crud-submit-edit">Submit</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>









@endsection


@section('script')

    <script>
        $(document).ready(function () {



                $('.datatable').DataTable(

                        {
                            "order": [[ 0, "desc" ]],
                            "iDisplayLength": 15,
                            "lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],


                            "columnDefs": [

                                { "sortable": false,  "targets": [ 5 ] }
                            ]

                        }



                );





            var page = 1;
            var current_page = 1;
            var total_page = 0;
            var is_ajax_fire = 0;

            /* Create new Item */
            /*    $(".crud-submit").click(function(e){
             e.preventDefault();
             var form_action = $("#create-item").find("form").attr("action");
             var title = $("#create-item").find("input[name='title']").val();
             var description = $("#create-item").find("textarea[name='description']").val();

             if(title != '' && description != ''){
             $.ajax({
             dataType: 'json',
             type:'POST',
             url: url + form_action,
             data:{title:title, description:description}
             }).done(function(data){
             $("#create-item").find("input[name='title']").val('');
             $("#create-item").find("textarea[name='description']").val('');
             getPageData();
             $(".modal").modal('hide');
             toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 5000});
             });
             }else{
             alert('You are missing title or description.')
             }


             });*/

            /* Remove Item */
            $("body").on("click", ".remove-item", function () {
                var id = $(this).parent("td").data('id');
                var c_obj = $(this).parents("tr");
                var token = $("#edit-item").find("input[name='_token']").val();
                $.ajax({
                    dataType: 'json',
                    type: 'delete',
                    url: '/admin/order/' + id,
                    data: {id: id, _token:token}
                }).done(function (data) {
                    c_obj.remove();
                    toastr.success('Item Deleted Successfully.', 'Success Alert', {timeOut: 5000});

                });

            });


            /* Edit Item */
            $("body").on("click", ".edit-item", function () {

                var id = $(this).parent("td").data('id');

                $.getJSON('/admin/order/' + id, function (data) {

                    if (data.error == false) {
                        $("#edit-item").find(".edit-id").val(id);
                        $("#edit-item").find("input[name='refNumber']").val(data.order.refNumber);
                        $("#edit-item").find("input[name='userEmail']").val(data.order.userEmail);
                        $("#edit-item").find("textarea[name='note']").val(data.order.note);
                        $("#edit-item").find("select[name='status']").val(data.order.status);
                        $("#edit-item").find("input[name='OrderTotal']").val(data.order.OrderTotal);


                    }


                });


            });


            /* Updated new Item */
            $(".crud-submit-edit").click(function (e) {

                e.preventDefault();
                var form_action = $("#edit-item").find("form").attr("action");
                var id = $("#edit-item").find(".edit-id").val();

                var refNumber = $("#edit-item").find("input[name='refNumber']").val();
                var userEmail = $("#edit-item").find("input[name='userEmail']").val();
                var note = $("#edit-item").find("textarea[name='note']").val();
                var status = $("#edit-item").find("select[name='status']").val();
                var OrderTotal = $("#edit-item").find("input[name='OrderTotal']").val();

                var token = $("#edit-item").find("input[name='_token']").val();

                var data = {
                    refNumber: refNumber,
                    userEmail: userEmail,
                    note: note,
                    status: status,
                    OrderTotal: OrderTotal,
                    _token: token
                };

                $.ajax({
                    dataType: 'json',
                    type: 'PUT',
                    url: form_action + '/' + id,
                    data: data
                }).done(function (data) {

                    if(data.error==false){
                        $(".modal").modal('hide');
                        toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 5000});
                    }else{
                        toastr.warning(data.msg, 'Success Alert', {timeOut: 5000});
                    }

                });


            });
        });


    </script>


@endsection