@extends('admin.base')

@section('container')




    <div class="row">

        <div class="col-sm-4">
            refNumber
        </div>

        <div class="col-sm-8">
            {{$order->refNumber}}
        </div>


    </div>
<h3>Items</h3>

    <table class="table">
        <thead>
        <tr>
            <th>ItemId</th>
            <th>Description</th>
            <th>Price</th>
            <th>TotalPrice</th>
            <th>Qty</th>
        </tr>
        </thead>
        <tbody>




        @foreach ($order->items as $item)


            <tr>
                <td>
                    {{$item->number}}
                </td>
                <td>
                    {{$item->description}}
                </td>
                <td>
                    {{$item->price}}
                </td>
                <td>
                    {{$item->TotalPrice}}
                </td>
                <td>
                    {{$item->quantity}}
                </td>

            </tr>


        @endforeach




        </tbody>

    </table>


@endsection

