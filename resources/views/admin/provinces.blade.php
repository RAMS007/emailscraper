@extends('admin.base')

@section('container')




    <button class="btn btn-info " data-toggle="modal" data-target="#create-item">Add</button>



    <table class="table table-bordered">
        <thead>
        <tr>
            <th>id</th>
            <th>Country</th>
            <th>Province</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($provinces as $province)

            <tr>
                <td>{{$province->id}}</td>
                <td>{{$province->Country}}</td>
                <td>{{$province->Province}}</td>
                <td data-id="{{$province->id}}">
                    <button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button>
                    <button class="btn btn-danger remove-item">Delete</button>
                </td>
            </tr>



        @endforeach


        </tbody>
    </table>


    <!-- Create Item Modal -->
    <div class="modal fade" id="create-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Item</h4>
                </div>

                <div class="modal-body">
                    <form data-toggle="validator" action="/admin/province/create" method="POST">

                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label" for="country">Country:</label>
                            <input type="text" name="Country" class="form-control" data-error="Please enter title."
                                   required/>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="province">Province:</label>
                            <input type="text" name="Province" class="form-control" data-error="Please enter province."
                                   required/>
                            <div class="help-block with-errors"></div>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn crud-submit btn-success">Submit</button>
                        </div>

                    </form>

                </div>
            </div>

        </div>
    </div>

    <!-- Edit Item Modal -->
    <div class="modal fade" id="edit-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
                </div>

                <div class="modal-body">
                    <form data-toggle="validator" action="/admin/province" method="put">
                        <input type="hidden" name="id" class="edit-id">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label" for="country">Country:</label>
                            <input type="text" name="Country" class="form-control" data-error="Please enter title."
                                   required/>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="province">Province:</label>
                            <input type="text" name="Province" class="form-control" data-error="Please enter province."
                                   required/>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success crud-submit-edit">Submit</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>










@endsection


@section('script')

    <script>
        $(document).ready(function () {

            /* Create new Item */
            $(".crud-submit").click(function (e) {
                e.preventDefault();
                var form_action = $("#create-item").find("form").attr("action");
                var country = $("#create-item").find("input[name='Country']").val();
                var province = $("#create-item").find("input[name='Province']").val();


                var token = $("#create-item").find("input[name='_token']").val();

                var data = {
                    Country: country,
                    Province: province,
                    _token: token
                };

                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: form_action,
                    data: data
                }).done(function (data) {


                    $(".modal").modal('hide');
                    toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 5000});
                });


            });

            /* Remove Item */
            $("body").on("click", ".remove-item", function () {
                var id = $(this).parent("td").data('id');
                var c_obj = $(this).parents("tr");
                var token = $("#edit-item").find("input[name='_token']").val();
                $.ajax({
                    dataType: 'json',
                    type: 'delete',
                    url: '/admin/province/' + id,
                    data: {id: id, _token: token}
                }).done(function (data) {
                    c_obj.remove();
                    toastr.success('Item Deleted Successfully.', 'Success Alert', {timeOut: 5000});

                });

            });


            /* Edit Item */
            $("body").on("click", ".edit-item", function () {
                var id = $(this).parent("td").data('id');
                $.getJSON('/admin/province/' + id, function (data) {
                    if (data.error == false) {
                        $("#edit-item").find(".edit-id").val(id);
                        $("#edit-item").find("input[name='Country']").val(data.province.Country);
                        $("#edit-item").find("input[name='Province']").val(data.province.Province);

                    }
                });
            });


            /* Updated new Item */
            $(".crud-submit-edit").click(function (e) {
                e.preventDefault();
                var form_action = $("#edit-item").find("form").attr("action");
                var id = $("#edit-item").find(".edit-id").val();

                var country = $("#edit-item").find("input[name='Country']").val();
                var province = $("#edit-item").find("input[name='Province']").val();


                var token = $("#edit-item").find("input[name='_token']").val();

                var data = {
                    Country: country,
                    Province: province,
                    _token: token
                };

                $.ajax({
                    dataType: 'json',
                    type: 'PUT',
                    url: form_action + '/' + id,
                    data: data
                }).done(function (data) {
                    $(".modal").modal('hide');
                    toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 5000});
                });
            });
        });

    </script>


@endsection