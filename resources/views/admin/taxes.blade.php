@extends('admin.base')

@section('container')




        <button class="btn btn-info "   data-toggle="modal" data-target="#create-item" >Add</button>



            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>id</th>
                    <th>canadianTax</th>
                    <th>provincialTax</th>
                    <th>Province</th>

                    <th >Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($taxes as $tax)

                    <tr>
                        <td>{{$tax->id}}</td>
                        <td>{{$tax->canadianTax}}</td>
                        <td>{{$tax->provincialTax}}</td>
                        <td>{{$tax->Province->Province}}</td>
                        <td data-id="{{$tax->id}}">
                            <button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button>
                            <button class="btn btn-danger remove-item">Delete</button>
                        </td>
                    </tr>



                    @endforeach



                </tbody>
            </table>


    <!-- Create Item Modal -->
    <div class="modal fade" id="create-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Item</h4>
                </div>

                <div class="modal-body">
                    <form data-toggle="validator" action="/admin/tax/create" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label" for="canadianTax">canadianTax :</label>
                            <input type="text" name="canadianTax" class="form-control" data-error="Please enter canadianTax." required />
                            <div class="help-block with-errors"></div>
                        </div>


                        <div class="form-group">
                            <label class="control-label" for="provincialTax">provincialTax :</label>
                            <input type="text" name="provincialTax" class="form-control" data-error="Please enter provincialTax." required />
                            <div class="help-block with-errors"></div>
                        </div>


                        <div class="form-group col-md-6">
                            <label class="control-label" for="Province">    Province</label>
                            <div class="controls">
                                <select id="ProvinceNew" name="Province" class=" form-control">
                                    <option>Please Select</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn crud-submit btn-success">Submit</button>
                        </div>

                    </form>

                </div>
            </div>

        </div>
    </div>

    <!-- Edit Item Modal -->
    <div class="modal fade" id="edit-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
                </div>

                <div class="modal-body">
                    <form data-toggle="validator" action="/admin/tax" method="put">
                        <input type="hidden" name="id" class="edit-id">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="control-label" for="canadianTax">canadianTax :</label>
                            <input type="text" name="canadianTax" class="form-control" data-error="Please enter canadianTax." required />
                            <div class="help-block with-errors"></div>
                        </div>


                        <div class="form-group">
                            <label class="control-label" for="provincialTax">provincialTax :</label>
                            <input type="text" name="provincialTax" class="form-control" data-error="Please enter provincialTax." required />
                            <div class="help-block with-errors"></div>
                        </div>


                        <div class="form-group col-md-6">
                            <label class="control-label" for="Province">    Province</label>
                            <div class="controls">
                                <select id="Province" name="Province" class=" form-control">
                                    <option>Please Select</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-success crud-submit-edit">Submit</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>




@endsection


@section('script')

    <script>
        $(document).ready(function () {


            $.getJSON('/coutryRegions?country=Canada', {customerId: $(this).val()}, function (data) {
                var options = '';
                for (var x = 0; x < data.length; x++) {
                    options += '<option value="' + data[x]['id'] + '">' + data[x]['Province'] + '</option>';
                }
                $('#Province').html(options);
                $('#ProvinceNew').html(options);

            });













            /* Create new Item */
            $(".crud-submit").click(function (e) {
                e.preventDefault();
                var form_action = $("#create-item").find("form").attr("action");
                var canadianTax = $("#create-item").find("input[name='canadianTax']").val();
                var provincialTax = $("#create-item").find("input[name='provincialTax']").val();
                var province = $("#create-item").find("select[name='Province']").val();


                var token = $("#create-item").find("input[name='_token']").val();

                var data = {
                    canadianTax: canadianTax,
                    provincialTax: provincialTax,
                    Province: province,
                    _token: token
                };

                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: form_action,
                    data: data
                }).done(function (data) {


                    $(".modal").modal('hide');
                    toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 5000});
                });


            });

            /* Remove Item */
            $("body").on("click", ".remove-item", function () {
                var id = $(this).parent("td").data('id');
                var c_obj = $(this).parents("tr");
                var token = $("#edit-item").find("input[name='_token']").val();
                $.ajax({
                    dataType: 'json',
                    type: 'delete',
                    url: '/admin/tax/' + id,
                    data: {id: id, _token: token}
                }).done(function (data) {
                    c_obj.remove();
                    toastr.success('Item Deleted Successfully.', 'Success Alert', {timeOut: 5000});

                });

            });


            /* Edit Item */
            $("body").on("click", ".edit-item", function () {
                var id = $(this).parent("td").data('id');
                $.getJSON('/admin/tax/' + id, function (data) {
                    if (data.error == false) {
                        $("#edit-item").find(".edit-id").val(id);
                        $("#edit-item").find("input[name='canadianTax']").val(data.tax.canadianTax);
                        $("#edit-item").find("input[name='provincialTax']").val(data.tax.provincialTax);
                        $("#edit-item").find("select[name='Province']").val(data.tax.provinceId);

                    }
                });
            });


            /* Updated new Item */
            $(".crud-submit-edit").click(function (e) {
                e.preventDefault();
                var form_action = $("#edit-item").find("form").attr("action");
                var id = $("#edit-item").find(".edit-id").val();

                var canadianTax = $("#edit-item").find("input[name='canadianTax']").val();
                var provincialTax = $("#edit-item").find("input[name='provincialTax']").val();

                var province = $("#edit-item").find("select[name='Province']").val();


                var token = $("#edit-item").find("input[name='_token']").val();

                var data = {
                    canadianTax: canadianTax,
                    provincialTax: provincialTax,

                    Province: province,
                    _token: token
                };

                $.ajax({
                    dataType: 'json',
                    type: 'PUT',
                    url: form_action + '/' + id,
                    data: data
                }).done(function (data) {
                    $(".modal").modal('hide');
                    toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 5000});
                });
            });
        });

    </script>


@endsection