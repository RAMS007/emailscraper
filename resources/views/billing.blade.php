<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        h2.bg-success {
            padding: 15px;
        }

        .required-lbl {
            color: red;
        }

        label[for="billinginformation"] {
            display: inline;
            float: left;
            margin: 0px 45px 0px 0px;
        }

        .card-expiry {
            padding-left: 0px;
        }

        .confirm-btn {
            float: right;
        }

        .bg-primary {
            padding: 10px;
        }

        label {
            margin-bottom: 0px;
        }
    </style>
</head>
<body>
<div class="container">


  <!--  <script src='https://libs.na.bambora.com/customcheckout/0.1.0/customcheckout.js'></script> -->


    <div class="row">
        <h2 class="bg-success">Order Total: $<span id="total">{{$Order->OrderTotal}} </span> </h2>
        <h2 class="bg-success">Tax: $<span id="tax"> </span> </h2>
        <h2 class="bg-success">Shipping: $ <span id="shipping"> </span>  </h2>
        <h2 class="bg-success">Total: $ <span id="GrandTotal"> </span>  </h2>



        <div class="form-group col-md-12 bg-primary">
            <label class="control-label" for="billinginformation">Shipping Information</label>
        </div>

<form method="POST" action="/makepayment">
    <input type="hidden" name="token" value="{{$Order->hashedEmailId}}">
    <input type="hidden" name="amount" value="0" id="amount">

        <div class="shipping-info">
            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="firstname">First Name</label>
                <div class="controls">
                    <input id="firstname" name="firstname" type="text" placeholder="" class="form-control" required="">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="lastname">Last Name</label>
                <div class="controls">
                    <input id="lastname" name="lastname" type="text" placeholder="" class="form-control" required="">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="shippingaddress">Shipping Address
                </label>
                <div class="controls">
                    <input id="shippingaddress" name="shippingaddress" type="text" placeholder="" class="form-control"
                           required="">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label" for="country">Country
                </label>
                <div class="controls">
                    <input id="country" name="country" type="text" placeholder="" class="form-control"
                           value="Canada">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="shippingstate">Shipping
                    State</label>
                <div class="controls">
                    <select id="shippingstate" name="shippingstate" class=" form-control">
                        <option>Please Select</option>
                    </select>
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="postcode">Post Code</label>
                <div class="controls">
                    <input id="postcode" name="postcode" type="text" placeholder="" class="form-control" required="">
                </div>
            </div>


            <hr/>
            <!------------------------------------------------>


            <div class="form-group col-md-12 bg-primary">
                <div class="control-group">

                    <div class="controls">
                        <label class="control-label" for="billinginformation">Billing Information</label>
                        <label class="checkbox" for="billinginformation">
                            <input type="checkbox" name="billinginformation" id="billinginformation"
                                   value="true" onchange="checkBilling()">
                            Use Shipping Address
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="firstnameB">First Name</label>
                <div class="controls">
                    <input id="firstnameB" name="firstnameB" type="text" placeholder="" class="form-control"
                           required="">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="lastnameB">Last Name</label>
                <div class="controls">
                    <input id="lastnameB" name="lastnameB" type="text" placeholder="" class="form-control" required="">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="shippingaddressB">Shipping Address
                </label>
                <div class="controls">
                    <input id="shippingaddressB" name="shippingaddressB" type="text" placeholder="" class="form-control"
                           required="">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label" for="countryB">Country
                </label>
                <div class="controls">
                    <input id="countryB" name="countryB" type="text" placeholder="" class="form-control"
                           value="Canada">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="shippingstateB">Shipping
                    State</label>
                <div class="controls">
                    <select id="shippingstateB" name="shippingstateB" class=" form-control">
                        <option>Please Select</option>

                    </select>
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="postcodeB">Post Code</label>
                <div class="controls">
                    <input id="postcodeB" name="postcodeB" type="text" placeholder="" class="form-control" required="">
                </div>
            </div>



            <hr/>


            <div class="form-group col-md-12">
                <div class="control-group confirm-btn">
                    <label class="control-label" for="placeorderbtn"></label>
                    <div class="controls">
                        <button id="pay-button" type="submit" name="placeorderbtn" class="btn btn-primary">Place My Order</button>

                    </div>
                </div>
            </div>

        </div>
</form>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/jquery-3.2.1.min.js"><\/script>')</script>
<script src="/js/bootstrap.min.js"></script>


<script>

    var taxes = [];
    var ShippingCosts=[];
    var totalOrder=0;
    var garndTotal=0;
    $(document).ready(function () {
         totalOrder= parseFloat($('#total').html());
         garndTotal = totalOrder;
        $.getJSON('/coutryRegions?country=Canada', {customerId: $(this).val()}, function (data) {
            var options = '';
            for (var x = 0; x < data.length; x++) {
                options += '<option value="' + data[x]['id'] + '">' + data[x]['Province'] + '</option>';
            }
            $('#shippingstate').html(options);
            $('#shippingstateB').html(options);


            if(taxes.length==0){

                setTimeout( recalculateTax, 2000);


            }else{
                recalculateTax();

            }




        });

        $.getJSON('/getTaxes', function (data) {
            if (data.error == false) {
                taxes = data.taxes;
                console.log(taxes);
            } else {
                console.log(data.msg);
            }

        });

        $.getJSON('/getShipping', function (data) {
            if (data.error == false) {
                ShippingCosts = data.shipping;

            } else {
                console.log(data.msg);
            }

        });



        $("#shippingstate").change(function () {
            recalculateTax();
        });


    })

    function recalculateTax (){




        console.log($("#shippingstate").val());
        var tax = taxes[$("#shippingstate").val()];
        console.log(tax);
        console.log(taxes);
        console.log($("#shippingstate").val());

        var taxAmount = totalOrder * (parseFloat(tax.canadianTax) + parseFloat(tax.provincialTax)) / 100 ;
       // garndTotal =totalOrder+ taxAmount;

        var shipping =0;


       for(i=0;i<ShippingCosts.length;i++){
console.log(ShippingCosts[i]);


           if((parseFloat(ShippingCosts[i].lowPrice)<= totalOrder) ){
               console.log('true');
           }
           if( (parseFloat(ShippingCosts[i].highPrice) >= totalOrder) ){
               console.log('true');
           }
           if( ( parseInt(ShippingCosts[i].provinceId) == parseInt($('#shippingstate').val()))){
               console.log('true');
           }else{
               console.log(parseInt($('#shippingstate').val()));
           }


           if((parseFloat(ShippingCosts[i].lowPrice)<= totalOrder) && (parseFloat(ShippingCosts[i].highPrice) >= totalOrder) && ( parseInt(ShippingCosts[i].provinceId) == parseInt($('#shippingstate').val()))){
               shipping =parseFloat( ShippingCosts[i].shippingPrice);
           }




       }

        garndTotal =totalOrder+ taxAmount+shipping;









        $('#tax').html( round(taxAmount,2));
        $('#GrandTotal').html(round(garndTotal,2));
        $('#amount').val(round(garndTotal,2));
        $('#shipping').html(round(shipping,2));


    }


    function checkBilling() {
        if ($('#billinginformation').is(':checked')) {

            $('#firstnameB').val($('#firstname').val());
            $('#firstnameB').prop("disabled", true);

            $('#lastnameB').val($('#lastname').val());
            $('#lastnameB').prop("disabled", true);

            $('#shippingaddressB').val($('#shippingaddress').val());
            $('#shippingaddressB').prop("disabled", true);


            $('#shippingstateB').val($('#shippingstate').val());
            $('#shippingstateB').prop("disabled", true);

            $('#postcodeB').val($('#postcode').val());
            $('#postcodeB').prop("disabled", true);

            $('#countryB').val($('#country').val());
            $('#countryB').prop("disabled", true);

            console.log('checked');
        } else {
            $('#firstnameB').prop("disabled", false);
            $('#lastnameB').prop("disabled", false);
            $('#shippingaddressB').prop("disabled", false);
            $('#shippingstateB').prop("disabled", false);
            $('#postcodeB').prop("disabled", false);
            $('#countryB').prop("disabled", false);

            console.log('un');

        }
    }


    function round(value, exp) {
        if (typeof exp === 'undefined' || +exp === 0)
            return Math.round(value);

        value = +value;
        exp = +exp;

        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
            return NaN;

        // Shift
        value = value.toString().split('e');
        value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
    }


    ///////////////////////////////

/*
    // initialize the library
    var customCheckout = customcheckout();

    // Create and mount the inputs with the library
    customCheckout.create('card-number').mount('#card-number');
    customCheckout.create('cvv').mount('#card-cvv');
    customCheckout.create('expiry').mount('#card-expiry');

    // listen for submit button
    document.getElementById('pay-button').onclick = function () {
        console.log('click');
        customCheckout.createToken(function (result) {
            if (result.error) {
                console.log(result.error.message);
                // display error message
            } else {
                console.log(result.token);

                var token = result.token;
                var name = $('#firstnameB').val();
                var amount = $('#GrandTotal').val();
                // process token using our payments api
                var data = {
                    "name": name,
                    "amount": amount,
                    "token": token
                };
                /*       $.ajax({
                 type: "POST",
                 url: '/makepayment',
                 data: data,
                 success: success,
                 dataType: json
                 });*/

/*
                $.post("/makepayment", data, function (data) {
                    alert("Data Loaded: " + data);
                });


            }
        });
    };

    // Add an event listener for when the inputs are complete
    customCheckout.on('complete', function (event) {
        if (event.field === 'card-number') {
            console.log('Card number is complete: ' + JSON.stringify(event));
        } else if (event.field === 'cvv') {
            console.log('CVV is complete: ' + JSON.stringify(event));
        } else if (event.field === 'expiry') {
            console.log('Expiry is complete: ' + JSON.stringify(event));
        }
    });

    // Add an event listener for when the inputs have a validation erro
    customCheckout.on('error', function (event) {
        if (event.field === 'card-number') {
            console.log('Card number has errors: ' + JSON.stringify(event));
        } else if (event.field === 'cvv') {
            console.log('CVV has errors: ' + JSON.stringify(event));
        } else if (event.field === 'expiry') {
            console.log('Expiry has errors: ' + JSON.stringify(event));
        }
    });


    /*


     function    makeTokenPayment  (token) {
     var self = this;

     console.log('checkout.makeTokenPayment()');

     var amount = document.getElementById('amount').value;
     var name = document.getElementById('name').value;

     var xhr = new XMLHttpRequest();
     var method = "POST";
     var url = "/payment/basic/token";

     if (document.getElementById('3ds-checkbox') !== null) {
     if (document.getElementById('3ds-checkbox').checked) {
     url = "/payment/enhanced/3d-secure/token";
     }
     else {
     url = "/payment/enhanced/token";
     }
     }

     var data = JSON.stringify({
     "name": name,
     "amount": amount,
     "token": token
     }, undefined, 2);

     setDisplayedRequest(data);

     xhr.open(method, url, true);
     xhr.onreadystatechange = function () {
     if (xhr.readyState === XMLHttpRequest.DONE) {
     var json = null;
     var jsonStr = '';

     try {
     json = JSON.parse(xhr.responseText);
     jsonStr = JSON.stringify(json, undefined, 2);
     setDisplayedResponse(jsonStr);
     }
     catch (ex) {
     console.log(ex);
     setDisplayedResponse(ex.message);
     }

     if (json !== null && jsonStr !== '' && xhr.status === 302) {
     var contents = json.contents;
     var contents_decoded = decodeURIComponent(contents.replace(/\+/g, '%20'));
     var formStr = contents_decoded.match("<FORM(.*)<\/FORM>");
     document.body.insertAdjacentHTML('afterbegin', formStr);
     document.form1.submit();
     } else {
     var message;
     if (xhr.status === 200) {
     var orderNumber = JSON.parse(xhr.responseText).order_number;
     var transactionId = JSON.parse(xhr.responseText).id;
     message = 'Made a payment using token.</br></br>Transaction Id:</br>' + transactionId + '</br></br>Order Number:</br>' + orderNumber;
     self.showSuccessFeedback(message);
     }
     else {
     message = xhr.responseText;
     try {
     message = JSON.parse(xhr.responseText).message;
     }
     catch (ex) {
     console.log('Parsing exception: ' + ex.message);
     }
     self.showErrorFeedback(message);
     }
     self.setPayButton(true);
     self.toggleProcessingScreen();
     }

     }
     }.bind(this);

     xhr.setRequestHeader("Content-Type", "application/json");
     xhr.send(data);
     },
     };



     */


</script>


</body>
</html>
