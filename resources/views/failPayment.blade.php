<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        h2.bg-success {
            padding: 15px;
        }

        .required-lbl {
            color: red;
        }

        label[for="billinginformation"] {
            display: inline;
            float: left;
            margin: 0px 45px 0px 0px;
        }

        .card-expiry {
            padding-left: 0px;
        }

        .confirm-btn {
            float: right;
        }

        .bg-primary {
            padding: 10px;
        }

        label {
            margin-bottom: 0px;
        }
    </style>
</head>
<body>
<div class="container">

    <div class="alert alert-danger">
        <strong>Payment Fail</strong>
    </div>


</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/jquery-3.2.1.min.js"><\/script>')</script>
<script src="/js/bootstrap.min.js"></script>


</body>
</html>
