<?php




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/ProcessEmails','OrdersController@ProcessEmails');
Route::get('/billing','OrdersController@showBilling');

Route::get('/coutryRegions','OrdersController@getProvinces');
Route::get('/getTaxes','OrdersController@getTaxes');
Route::get('/getShipping','OrdersController@getShipping');

Route::post('/makepayment','OrdersController@makePayment');


Route::get('/payments/result/{hash}','PaymentsController@processResponse');
Route::get('/payments/fail',function(){
    return view('failPayment');
});

Route::get('/payments/success',function(){
    return view('successPayment');
});


Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@dashboard');
    Route::get('/order/{orderId}', 'AdminController@getOrderDetails');
    Route::get('/order/details/{orderId}', 'AdminController@getAllOrderDetails');
    Route::put('/order/{orderId}', 'AdminController@updateOrderDetails');
    Route::delete('/order/{orderId}', 'AdminController@deleteOrder');


    Route::get('/provinces', 'AdminController@getProvinces');
    Route::get('/province/{provinceId}', 'AdminController@getAllProvinceDetails');
    Route::put('/province/{provinceId}', 'AdminController@updateProvinceDetails');
    Route::post('/province/create', 'AdminController@createProvince');
    Route::delete('/province/{provinceId}', 'AdminController@deleteProvince');


    Route::get('/taxes', 'AdminController@getTaxes');
    Route::get('/tax/{taxId}', 'AdminController@getAllTaxDetails');
    Route::put('/tax/{taxId}', 'AdminController@updateTaxDetails');
    Route::post('/tax/create', 'AdminController@createTax');
    Route::delete('/tax/{taxId}', 'AdminController@deleteTax');

    Route::get('/shipping', 'AdminController@getShipping');
    Route::get('/shipping/{shippingId}', 'AdminController@getAllshippingDetails');
    Route::put('/shipping/{shippingId}', 'AdminController@updateshippingDetails');
    Route::post('/shipping/create', 'AdminController@createShiipping');
    Route::delete('/shipping/{shippingId}', 'AdminController@deleteshipping');



});